module code.gitea.io/actions-proto-go

go 1.19

require (
	github.com/bufbuild/connect-go v1.3.1
	google.golang.org/protobuf v1.28.1
)
